class Sudoku {
    public:
        // les attributs sont laissés public pour simplifier mais on pourrait les rendre privés et créer des accesseurs
        int taille;        
        int **grille;

        Sudoku();

        /** méthode permettant de check si un chiffre est possible pour une case précise dans une ligne
         * entrée : un numéro de ligne, un chiffre
         * sortie : un booléen, vrai si chiffre possible, faux sinon
        */
        bool checkRow(int, int);

        /** méthode permettant de check si un chiffre est possible pour une case précise dans une colonne
         * entrée : un numéro de colonne, un chiffre
         * sortie : un booléen, vrai si chiffre possible, faux sinon
        */
        bool checkCol(int, int);

        /** méthode permettant de check si un chiffre est possible pour une case précise dans une grille
         * entrée : un numéro de ligne, un numéro de colonne, un chiffre
         * sortie : un booléen, vrai si chiffre possible, faux sinon
        */
        bool checkGrid(int, int, int);

        /** méthode permettant de check si un chiffre est possible pour une case précise selon la ligne, la colonne et la grille où elle se trouve
         * entrée : un numéro de ligne, un numéro de colonne, un chiffre
         * sortie : un booléen, vrai si chiffre possible, faux sinon
        */
        bool isSafe(int, int, int);

        /** méthode récursive appliquant le principe du backtracking pour résoudre un sudoku 
         * entrée : un numéro de ligne, un numéro de colonne
         * sortie : un booléen, vrai si solution trouvée, faux sinon
        */
        bool solveSudoku(int, int);
};
