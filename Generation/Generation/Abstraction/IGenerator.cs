﻿namespace Generation.Abstraction
{
    public interface IGenerator
    {
        World Generate(GenerationParameters parameters);
        World Populate(World world, GenerationParameters parameters);
    }
}
