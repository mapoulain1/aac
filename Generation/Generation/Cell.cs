﻿namespace Generation
{
    public struct Cell
    {
        public CellType Type { get; set; }

        public Cell(CellType type = CellType.None) => Type = type;

        public override string ToString()
        {
            return Type switch
            {
                CellType.None => "0",
                CellType.DeepWater => "1",
                CellType.Water => "2",
                CellType.Shore => "3",
                CellType.Beach => "4",
                CellType.Grass => "5",
                CellType.Hill => "6",
                CellType.Mountain => "7",
                CellType.ExtremeMountain => "8",
                CellType.House => "9",
                CellType.Path => "A",
                _ => "0"
            };
        }
    }
}
