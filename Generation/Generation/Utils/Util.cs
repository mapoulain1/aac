﻿using System.Text;

namespace Generation.Utils
{
    internal static class Util
    {
        public static IList<string> Flatten(this Cell[,] cells)
        {
            var sb = new StringBuilder();
            var list = new List<string>();

            for (int i = 0; i < cells.GetLength(0); i++)
            {
                sb.Clear();
                for (int j = 0; j < cells.GetLength(1); j++)
                    sb.Append(cells[i, j].ToString());

                list.Add(sb.ToString());
            }

            return list;
        }

        public static bool IsSuitable(this CellType type) => type >= CellType.Beach && type <= CellType.Mountain;


        public static bool ValueInRange(int value, int min, int max) => value >= min && value <= max;

        public static bool RectangleOverlap(int Ax, int Ay, int Bx, int By, int size)
        {
            bool xOverlap = ValueInRange(Ax, Bx, Bx + size) ||
                            ValueInRange(Bx, Ax, Ax + size);

            bool yOverlap = ValueInRange(Ay, By, By + size) ||
                            ValueInRange(By, Ay, Ay + size);

            return xOverlap && yOverlap;
        }
    }
}
