﻿using System.Linq;
using Generation.Utils;

namespace Generation
{
    public class World
    {
        public int Width { get; }
        public int Height { get; }
        public IList<string> Data => Cells.Flatten();
        private Cell[,] Cells { get; set; }


        public World(int width, int height)
        {
            Width = width;
            Height = height;
            Cells = new Cell[Width, Height];

            for (var i = 0; i < Width; i++)
                for (var j = 0; j < Height; j++)
                    Cells[i, j] = new Cell();
        }

        public void SetCell(int i, int j, float height)
        {
            height = (height + 0.707f) * 256;
            Cells[i, j] = height switch
            {
                < 32 => new Cell(CellType.DeepWater),
                < 64 => new Cell(CellType.Water),
                < 96 => new Cell(CellType.Shore),
                < 120 => new Cell(CellType.Beach),
                < 168 => new Cell(CellType.Grass),
                < 192 => new Cell(CellType.Hill),
                < 224 => new Cell(CellType.Mountain),
                _ => new Cell(CellType.ExtremeMountain)
            };
        }

        public CellType GetCell(int i, int j)
        {
            if (i > 0 && j > 0 && i < Width && j < Height)
                return Cells[i, j].Type;
            return CellType.None;
        }

        public void SetCell(int i, int j, CellType type)
        {
            if (i > 0 && j > 0 && i < Width && j < Height)
                Cells[i, j].Type = type;
        }
    }
}
