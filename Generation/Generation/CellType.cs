﻿namespace Generation;

public enum CellType : short
{
    None,
    DeepWater,
    Water,
    Shore,
    Beach,
    Grass,
    Hill,
    Mountain,
    ExtremeMountain,
    House,
    Path
}