import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Settings } from '../model/settings';
import { NoiseType } from '../model/noise.type';
import { CustomPreset, Presets } from '../data/presets';
import { Preset } from '../model/preset';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})
export class ControlsComponent implements OnInit {
  @Output("settingsChanged")
  settingsChanged: EventEmitter<Settings>;

  minSize: number = 16;
  maxSize: number = 1024;

  minScale: number = 0.01;
  maxScale: number = 2.00;

  minFrequency: number = 0.00;
  maxFrequency: number = 0.15;

  minOctaveCount: number = 1;
  maxOctaveCount: number = 20;

  minLacunarity: number = 1.00;
  maxLacunarity: number = 5.00;

  minGain: number = 0.00;
  maxGain: number = 5.00;

  minDelta: number = -1.00;
  maxDelta: number = 1.00;

  minNumberOfVillages = 0;
  maxNumberOfVillages = 3;

  minHousesSize = 0;
  maxHousesSize = 5;

  minVillagesPopulation = 0;
  maxVillagesPopulation = 10;

  minVillagesSpread = 1;
  maxVillagesSpread = 50;

  NoiseTypes = [NoiseType.Value, NoiseType.ValueFractal,
  NoiseType.Perlin, NoiseType.PerlinFractal,
  NoiseType.Simplex, NoiseType.SimplexFractal,
  NoiseType.Cellular, NoiseType.WhiteNoise,
  NoiseType.Cubic, NoiseType.CubicFractal];

  presets = Presets;
  selectedPreset: Preset = CustomPreset;

  constructor() {
    this.settingsChanged = new EventEmitter<Settings>();
  }


  ngOnInit(): void {
    this.emit();
    this.emit();
  }

  presetChanged() {
    this.emit(false);
  }

  emit(resetPreset: boolean = true) {
    if (resetPreset) {
      this.selectedPreset = { name: CustomPreset.name, settings: { ... this.selectedPreset.settings } };
    }
    this.selectedPreset.settings.newSeed = false;
    this.settingsChanged.emit(this.selectedPreset.settings);
  }

  generate() {
    this.emit();
  }

  sizeChanged() {
    this.emit();
  }

  scaleChanged() {
    this.emit();
  }

  frequencyChanged() {
    this.emit();
  }

  octaveCountChanged() {
    this.emit();
  }

  lacunarityChanged() {
    this.emit();
  }

  gainChanged() {
    this.emit();
  }

  typeChanged() {
    this.emit();
  }

  deltaChanged() {
    this.emit();
  }

  numberOfVillagesChanged() {
    this.emit();
  }

  housesSizeChanged() {
    this.emit();
  }

  villagesPopulationChanged() {
    this.emit();
  }

  villagesSpreadChanged() {
    this.emit();
  }

  label(index: number): string {
    return NoiseType[index];
  }

}
