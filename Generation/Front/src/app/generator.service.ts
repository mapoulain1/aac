import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { World } from './model/world';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs'
import { Settings } from './model/settings';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {

  constructor(private client: HttpClient) { }

  get(settings: Settings): Observable<World> {
    let parameters = "?";
    let k: keyof typeof settings;
    for (k in settings){
      parameters += `${k}=${settings[k]}&`
    }

    return this.client.get<World>(`${environment.api}Generation${parameters}`);
  }

}
