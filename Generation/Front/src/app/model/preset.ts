import { Settings } from "./settings";

export class Preset {
  name: string = "";
  settings: Settings;

  constructor(settings: Settings) {
    this.settings = settings;
  }
}
