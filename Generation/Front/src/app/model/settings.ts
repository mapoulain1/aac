import { NoiseType } from "./noise.type";

export class Settings{
  size: number = 0;
  scale: number = 0;
  newSeed: boolean = false;
  frequency: number = 0;
  octaveCount: number = 0;
  persistence: number = 0;
  lacunarity: number = 0;
  gain: number = 0;
  delta: number = 0;
  noiseType: NoiseType = NoiseType.Value;
  numberOfVillages: number = 0;
  housesSize: number = 0;
  villagesPopulation: number = 0;
  villagesSpread: number = 0;
}
