export enum NoiseType {
  Value,
  ValueFractal,
  Perlin,
  PerlinFractal,
  Simplex,
  SimplexFractal,
  Cellular,
  WhiteNoise,
  Cubic,
  CubicFractal
}
