import { Component, OnInit } from '@angular/core';
import Point from './model/point';
import Rectangle from './model/rectangle';
import { RectangleService } from './rectangle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  rows: number = 12;
  cols: number = 24;

  values: boolean[][] = [];
  rectangles: Rectangle[] = [];

  constructor(private rectangleService : RectangleService){}

  ngOnInit(): void {
    this.values = new Array(this.cols).fill(false).map(() =>
      new Array(this.rows).fill(false)
    );
  }

  n(n: number): Array<number> {
    return Array.from(Array(n).keys());
  }

  stateChange(i: number, j: number, event: boolean) {
    this.values[i][j] = event;
    let points: Point[] = [];
    for (var i = 0; i < this.values.length; i++) {
      let row = this.values[i];
      for (var j = 0; j < row.length; j++) {
        if(row[j])
          points.push(new Point(i, j));
      }
    }

    this.rectangleService.process(points).subscribe(x => {
      this.rectangles = x;
    })
  }

}
