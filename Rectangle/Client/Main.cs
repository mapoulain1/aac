﻿using RectangleLib;
using RectangleLib.Implementation;

var calculator = new RectanglerCalculator();
var points = new List<Point>
{
    new(0, 0),
    new(0, 4),
    new(4, 4),
    new(4, 0),
    new(-4, 2),
    new(-3, 0),
    new(1, 2),
    new(1, 3),
    new(-4, 0),
    new(-4, 4)
};

var result = calculator.Process(points);
Console.WriteLine(result.Count);
foreach (var rectangle in result)
{
    Console.WriteLine(rectangle);
}