﻿namespace RectangleLib.Abstraction
{
    public interface IRectangleCalculator
    {
        ICollection<Rectangle> Process(ICollection<Point> points);
    }
}
