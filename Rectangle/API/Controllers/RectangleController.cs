using Microsoft.AspNetCore.Mvc;
using RectangleLib;
using RectangleLib.Abstraction;

namespace API.Controllers
{
    [ApiController]
    [Route("api-rectangle/[controller]")]
    public class RectangleController : ControllerBase
    {
        private readonly IRectangleCalculator _rectangleCalculator;

        public RectangleController(IRectangleCalculator rectangleCalculator)
        {
            _rectangleCalculator = rectangleCalculator;
        }

        [HttpPost]
        public IActionResult Rectangles([FromBody] ICollection<Point> points)
        {
            return Ok(_rectangleCalculator.Process(points));
        }

    }
}